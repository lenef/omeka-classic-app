# Omeka Classic Cloudron app

This repository contains the Cloudron app package source for [Omeka Classic](https://omeka.org/classic/).

This app is experimental and needs to be extensively tested.

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/documentation/custom-apps/tutorial/#cloudron-cli).

```
cd omeka-classic-app

cloudron build
cloudron install
```

## Todo

- LDAP intégration
