Omeka Classic is pre-setup with an admin account. Go to the Admin dashboard and use this initial credentials to login at `/admin`:

```
- username: admin
- password: changeme
```

**Note:** Please change the password upon first login. 

To change the language of the administration interface and the default site, enable and configure the Switch Language plugin.

Themes and plugins can be installed automatically after activation of the Escher plugin. This can also be done manually via the terminal or using the Cloudron CLI tools.
