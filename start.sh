#!/bin/bash

set -eu

mkdir -p /run/php/sessions

export MYSQL_PWD=${CLOUDRON_MYSQL_PASSWORD} # This will avoid the mysql password warning
mysql="mysql --user=${CLOUDRON_MYSQL_USERNAME} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE}"

if [[ ! -d "/app/data/files" ]]; then
    cp -rf /app/code/files_vanilla /app/data/files
    chown -R www-data:www-data /app/data 
fi

if [[ ! -d "/app/data/themes" ]]; then
    mkdir /app/data/themes
    ln -sf /app/code/themes_vanilla/default /app/data/themes/default
    ln -sf /app/code/themes_vanilla/berlin /app/data/themes/berlin
    ln -sf /app/code/themes_vanilla/seasons /app/data/themes/seasons
fi

if [[ ! -d "/app/data/logs" ]]; then
    mkdir /app/data/logs
    cp /app/code/logs_vanilla/errors.log /app/data/logs
fi

if [[ ! -d "/app/data/plugins" ]]; then
    mkdir /app/data/plugins
    ln -sf /app/code/plugins_vanilla/Coins /app/data/plugins/Coins
    ln -sf /app/code/plugins_vanilla/ExhibitBuilder /app/data/plugins/ExhibitBuilder
    ln -sf /app/code/plugins_vanilla/SimplePages /app/data/plugins/SimplePages
fi

if [[ ! -L "/app/data/plugins/SwitchLanguage" ]]; then
    ln -sf /app/code/plugins_vanilla/SwitchLanguage /app/data/plugins/SwitchLanguage
fi

if [[ ! -L "/app/data/plugins/Escher" ]]; then
    ln -sf /app/code/plugins_vanilla/Escher /app/data/plugins/Escher
fi

if [[ ! -L "/app/data/plugins/CentralAuth" ]]; then
    ln -sf /app/code/plugins_vanilla/CentralAuth /app/data/plugins/CentralAuth
fi
if [[ ! -f /app/data/.initialized ]]; then
    echo "Starting apache for setup Omeka Classic"

    APACHE_CONFDIR="" source /etc/apache2/envvars
    rm -f "${APACHE_PID_FILE}"
    ( /usr/sbin/apache2 -DFOREGROUND ) &
    apache_pid=$!

    while [[ ! -f "/var/run/apache2/apache2.pid" ]]; do
        echo "Waiting for apache2 to start"
        sleep 1
    done

    $mysql < /app/code/mysql.sql

    curl -X POST "http://localhost:3000/install/install.php" \
        --data-urlencode "super_email=${CLOUDRON_MAIL_FROM}" \
        --data-urlencode "administrator_email=${CLOUDRON_MAIL_FROM}" \
        --data-urlencode "username=admin" \
        --data-urlencode "password=changeme" \
        --data-urlencode "password_confirm=changeme" \
        --data-urlencode "site_title=Omeka Classic installation" \
        --data-urlencode "description=Omeka Classic site " \
        --data-urlencode "tag_delimiter=','" \
        --data-urlencode "fullsize_constraint=800" \
        --data-urlencode "thumbnail_constraint=400" \
        --data-urlencode "square_thumbnail_constraint=200" \
        --data-urlencode "path_to_convert=/usr/bin/" \
        --data-urlencode "per_page_admin=10" \
        --data-urlencode "per_page_public=10"

    touch /app/data/.initialized
    echo "Setup Omeka Classic complete"
    kill ${apache_pid}
fi

chown -R www-data:www-data /app/data /run/php/sessions

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
