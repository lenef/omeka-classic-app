Omeka provides open-source two web publishing — Omeka Classic and Omeka S (Semantic) — platforms for sharing digital collections and creating media-rich online exhibits.

Omeka Classic is designed for individual projects and educators managing a single site around a general theme or a research. Omeka S was created for institutions that operate a set of resources that can be shared across multiple sites.  Omeka Classic uses Dublin Core, a small set of vocabulary terms, while Omeka S implements multiple vocabularies and facilitates open linked data.

Omeka Classic is licensed under GNU General Public License v3.0.
