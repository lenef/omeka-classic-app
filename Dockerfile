FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

RUN mkdir -p /app/code
WORKDIR /app/code

# Omeka Classic version
ARG VERSION=3.0.3

# install dependencies
RUN apt-get update && apt-get install -y php libapache2-mod-php crudini \
    rsync \
    git \
    imagemagick \
    && rm -rf /var/cache/apt /var/lib/apt/lists /etc/ssh_host_*

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
COPY apache/app.conf /etc/apache2/sites-enabled/app.conf
RUN echo "Listen 3000" > /etc/apache2/ports.conf

RUN a2enmod php7.4 rewrite

# configure mod_php
RUN crudini --set /etc/php/7.4/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.save_path /run/php/sessions && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_divisor 100

# download and install omeka classic & some plugins
RUN wget https://github.com/omeka/Omeka/releases/download/v${VERSION}/omeka-${VERSION}.zip \
    -O /tmp/omeka.zip && \
    unzip /tmp/omeka.zip -d /tmp 

RUN mv /tmp/omeka-${VERSION}/files /tmp/omeka-${VERSION}/files_vanilla
RUN mv /tmp/omeka-${VERSION}/themes /tmp/omeka-${VERSION}/themes_vanilla
RUN mv /tmp/omeka-${VERSION}/plugins /tmp/omeka-${VERSION}/plugins_vanilla
RUN mv /tmp/omeka-${VERSION}/application/logs /tmp/omeka-${VERSION}/logs_vanilla

RUN rsync -r /tmp/omeka-${VERSION}/ /app/code/
RUN rm -Rf /tmp/omeka*

RUN git clone --depth 1 https://gitlab.com/TIME_LAS/Omeka_Plugin_SwitchLang.git \
    /app/code/plugins_vanilla/SwitchLanguage

RUN wget https://github.com/AcuGIS/Escher/archive/2.2.zip -O /app/code/plugins_vanilla/2.2.zip 
RUN unzip /app/code/plugins_vanilla/2.2.zip -d /app/code/plugins_vanilla/ 
RUN mv /app/code/plugins_vanilla/Escher-2.2 /app/code/plugins_vanilla/Escher
RUN rm /app/code/plugins_vanilla/2.2.zip

#RUN wget https://github.com/BGSU-LITS/omeka-plugin-CentralAuth/releases/download/v1.5/CentralAuth-1.5.zip -O /app/code/plugins_vanilla/CentralAuth-1.5.zip 
#RUN unzip /app/code/plugins_vanilla/CentralAuth-1.5.zip -d /app/code/plugins_vanilla/ 
#RUN rm /app/code/plugins_vanilla/CentralAuth-1.5.zip
#COPY central_auth.ini /app/code/plugins_vanilla/CentralAuth/central_auth.ini

# move files and folders
COPY mysql.sql /app/code/
COPY .htaccess /app/code/
COPY db.ini /app/code/
COPY config.ini /app/code/application/config/
RUN ln -sf /app/data/files /app/code/files
RUN ln -sf /app/data/themes /app/code/themes
RUN ln -sf /app/data/plugins /app/code/plugins
RUN ln -sf /app/data/logs /app/code/application/logs

# add code
COPY start.sh /app/pkg/

RUN chown -R www-data.www-data /app/code

CMD [ "/app/pkg/start.sh" ]
